FROM debian
RUN apt-get update \
        && apt-get install -y maven default-jre default-jdk git wget openjdk-8-jdk nano

RUN git clone https://gitlab.com/ot-interview/javaapp.git
WORKDIR /javaapp
RUN mvn clean packages
RUN mkdir /usr/local/tomcat
RUN wget https://mirrors.estointernet.in/apache/tomcat/tomcat-8/v8.5.64/bin/apache-tomcat-8.5.64.tar.gz -O /tmp/tomcat.tar.gz
RUN cd /tmp && tar xvfz tomcat.tar.gz
RUN cp -Rv /tmp/apache-tomcat-8.5.64/* /usr/local/tomcat/
COPY /javaapp/target/Spring3HibernateApp.war /usr/local/tomcat/webapps/
EXPOSE 9090
CMD /usr/local/tomcat/bin/catalina.sh run
